# SsrProject

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.4.

## Development server

step1: Please make sure you are on t-mobile network/vpn
step2: npm install
step3: To run with server `npm run start`for a dev server.Navigate to `http://localhost:4000/`.
To run client side :Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## consuming digital Unav

step 1: npm i @tmo/digital-unav --save (need to be on internal network)
add "./node_modules/@tmo/digital-unav/lib/scss/styles.scss" to you global styles
setp 2: npm i @mdi/fonts
add "node_modules/@mdi/font/scss/materialdesignicons.scss" to you global styles

setp 3: add SharedDigitalUnavModule to you module with configuration

eg:  
 @NgModule({
.....

imports: [
......,
SharedDigitalUnavModule.forRoot({
baseUrl: environment.unavBaseUrl,
cartApiUrl: environment.cartApiUrl,
cartCookie: environment.cartCookie
})
]
})

step 4: add template selector   

eg: authroring controled extenal data call (baseed on baseUrl in config)
<tmo-digital-header></tmo-digital-header>
<tmo-digital-footer></tmo-digital-footer>

eg: for app controled authoring 
<tmo-digital-header [contentData]='authoringData'></tmo-digital-header>
<tmo-digital-footer [contentData]='authoringData'></tmo-digital-footer>
  