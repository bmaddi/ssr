import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';

import { AppModule } from './app.module';
import { AppComponent } from './app.component';
import { ModuleMapLoaderModule } from '@nguniversal/module-map-ngfactory-loader';
import { SharedDigitalUnavModule } from '@tmo/digital-unav';
import { environment } from '../environments/environment';

@NgModule({
  imports: [
    AppModule,
    ServerModule,
    ModuleMapLoaderModule,
    SharedDigitalUnavModule.forRoot({
      baseUrl: environment.unavBaseUrl,
      cartApiUrl: environment.cartApiUrl,
      cartCookie: environment.cartCookie
  })
  ],
  bootstrap: [AppComponent],
})
export class AppServerModule {}
