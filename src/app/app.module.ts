import { BrowserModule } from '@angular/platform-browser';
// import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SharedDigitalUnavModule } from '@tmo/digital-unav';
import { environment } from '../environments/environment';

// import { RouterModule, Routes } from '@angular/router';

// const appRoutes: Routes = [];

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }), SharedDigitalUnavModule.forRoot({
      baseUrl: environment.unavBaseUrl,
      cartApiUrl: environment.cartApiUrl,
      cartCookie: environment.cartCookie
  })
  ],

  providers: [SharedDigitalUnavModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
