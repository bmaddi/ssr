export const environment = {
  production: true,
  unavBaseUrl: 'https://testtmohome.s3-us-west-2.amazonaws.com/content',
  cartApiUrl: 'https://api.t-mobile.com/v2/carts/',
  cartCookie: 'tmobglobalshareddata'
};
